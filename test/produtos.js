var express = require('../config/express')();
var request = require('supertest')(express);
//var assert = require('assert');

describe('#PRODUTOS CONTROLLER',function(){
    beforeEach(function(done){
        var conn = express.infra.connectionFactory();
        conn.query("DELETE FROM TableBooks;",function(ex,result){
            if(!ex) {done();}
        });
    });

    it('#TESTE :: listagem json',function(done){
        request.get('/produtos').set('Accept','application/json').expect('Content-Type',/json/).expect(200,done);
    });

    it('#TESTE :: cadastro produto com dados invalidos',function(done){
        request.post('/produtos').send({nome:"",descricao:"novo livro"}).expect(400,done);
    });

    it('#TESTE :: cadastro produto com dados validos',function(done){
        request.post('/produtos').send({nome:"supertest",descricao:"novo livro",preco:200}).expect(302,done);
    });
});
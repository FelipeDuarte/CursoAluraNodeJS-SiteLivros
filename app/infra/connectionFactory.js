var mysql = require('mysql');

//process.env.NODE_ENV == 'test'
//process.env.NODE_ENV == 'development'
//process.env.NODE_ENV == 'production'

var connectMYSQL = function () {
    //if(process.env.NODE_ENV == 'development')
    if (!process.env.NODE_ENV) {
        console.log('---->criando conexão ::::::::::: MYSQL {localhost, root, ****, dbnodejs}');
        return mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'lipe_013',
            database: 'dbnodejs'
        });
    }

    if (process.env.NODE_ENV == 'test') {
        console.log('---->criando conexão ::::::::::: MYSQL {localhost, root, ****, dbnodejs_test}');
        return mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'lipe_013',
            database: 'dbnodejs_test'
        });
    }

    if (process.env.NODE_ENV == 'production') {
        //login | senha | host | nomeBD
        var urlHeroku = process.env.CLEARDB_DATABASE_URL;
        var data = urlHeroku.match(/mysql:\/\/(.*):(.*)@(.*)\/(.*)\?reconnect=true/);
        config = {
            login: data[1],
            senha: data[2],
            host: data[3],
            nomeDB: data[4],
        };

        console.log('---->criando conexão ::::::::::: MYSQL {host heroku, root, ****, database heroku}');
        return mysql.createConnection({
            host: config.host,
            user: config.login,
            password: config.senha,
            database: config.nomeDB
        });
    }
}

module.exports = function () {
    console.log('---->CALL ::::::::::: module.exports = connectionFactory');
    return connectMYSQL;
}
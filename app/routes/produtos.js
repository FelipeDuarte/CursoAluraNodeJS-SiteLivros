//var dbConnection = require('../infra/connectionFactory');

module.exports = function(app) {
    
    app.get('/produtos',function(req,res,next) {
        var connection = app.infra.connectionFactory();
        var produtosDAO = new app.infra.ProdutosDAO(connection);
        connection.connect(function(err) {
            console.log(err);
            //if(err) throw err;
            console.log("---->connected successful database mysql!!!");
        });
        produtosDAO.listar(function(errosL,restulados) {
            if(errosL){
                console.log('>>>>>>>>>>>>>>>>>>>>>>>>> erro listar');
                return next(errosL);
            }
            res.format({
                html: function(){
                    res.render('produtos/listaExib',{lista_tuplas:restulados});
                },
                json: function(){
                    res.json(restulados);
                }
            });
        });
        connection.end();
    });

    app.get('/produtos/form',function(req,res){
        res.render('produtos/form',{errosValidacao:{},prodf:{}});
    });

    app.post('/produtos',function(req,res){

        var produto = req.body;
        console.log("post :: salva");
        console.log(produto);

        req.assert('nome', 'Titulo é obrigatório').notEmpty();
        req.assert('preco', 'Formato inválido').isFloat();
        var erroVal = req.validationErrors();
        if(erroVal) {
            res.format({
                html: function(){
                    res.status(400).render('produtos/form',{errosValidacao:erroVal,prodf:produto});
                },
                json: function(){
                    res.status(400).json(erroVal);
                }
            });
            return;
        }
        
        var connection = app.infra.connectionFactory();
        var produtosDAO = new app.infra.ProdutosDAO(connection);

        produtosDAO.salvar(produto,function(erros,restulados){
            console.log(erros);
            //if(erros) throw erros;
            res.redirect('/produtos');
        });
    });
}
/**
 * fda
 */
//? comentario
//! comentario
//* comentario
//// comentarios
//TODO comentario
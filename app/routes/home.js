module.exports = function(app){
    app.get('/', function(req, res){
        var connection = app.infra.connectionFactory();
        var produtosDAO = new app.infra.ProdutosDAO(connection);
        produtosDAO.listar(function(erros,resultados){
            res.render('home/index',{livros:resultados}); //res.render('home/index');
        });
        connection.end();
    });
}